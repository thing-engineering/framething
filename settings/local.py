from .base import *

DEBUG = True

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

DATABASES = {
	'default': {
		'ENGINE': 'django.db.backends.postgresql_psycopg2',
		'NAME': 'framething',
		'HOST': 'datahaven',
	}
}

ALLOWED_HOSTS = ['*']

STATIC_ROOT = 'devstatic'

INTERNAL_IPS = ['192.168.0.%d' % (n) for n in range(1, 256)]

from .email import *
