import os

from .base import *

DEBUG = (os.getenv('PRODUCTION_DEBUG') is not None)

SECRET_KEY = os.environ['SECRET_KEY']

STATIC_ROOT = os.path.join(BASE_DIR, 'staticfiles')
STATIC_URL = '/static/'

TIME_ZONE = 'Etc/UTC'

ALLOWED_HOSTS = [
    'framething.org',
    'localhost',#testing
]

CSRF_COOKIE_SECURE = True
SESSION_COOKIE_SECURE = True

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': os.getenv('POSTGRES_DATABASE', 'framething'),
        'USER': os.getenv('POSTGRES_USER', ''),
        'PASSWORD': os.getenv('POSTGRES_PASSWORD', ''),
        'HOST': os.getenv('POSTGRES_HOST', ''),
        'PORT': os.getenv('POSTGRES_PORT', ''),
        'CONN_MAX_AGE': 300,
    }
}

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
        'LOCATION': '%s:%s' % (os.getenv('MEMCACHED_HOST', ''), os.getenv('MEMCACHED_PORT', '11211')),
        'KEY_PREFIX': 'framething',
    }
}

SOCIAL_AUTH_REDIRECT_IS_HTTPS = True

SOCIAL_AUTH_GOOGLE_OAUTH2_KEY = os.getenv('GOOGLE_OAUTH2_KEY', '')
SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET = os.getenv('GOOGLE_OAUTH2_SECRET', '')

COMPRESS_ENABLED = True
COMPRESS_OFFLINE = True

EMAIL_HOST = 'smtp-relay.things.svc.cluster.local'
EMAIL_PORT = 25
