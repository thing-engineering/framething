# Generated by Django 2.0.7 on 2018-07-29 19:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('data', '0003_auto_20180729_1954'),
    ]

    operations = [
        migrations.AlterField(
            model_name='item',
            name='unique_name',
            field=models.CharField(max_length=128, unique=True),
        ),
    ]
