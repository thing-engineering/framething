import json
import os

from django.core.management.base import BaseCommand, CommandError

from data.models import *

# -----------------------------------------------------------------------------

IMAGE_SRC_PATH = 'warframe-items/data/img'
BACKUP_SRC_PATH = ''
IMAGE_DST_PATH = 'work/resized'
OUT_PATH = 'static/js/generated_data.js'

IMAGE_OVERRIDES = {
    # Primary
    "antler-bow.png": "cernos.png",
    "grineer-flak-cannon.png": "drakgoon.png",
    "grineer-assault-rifle.png": "grakata.png",
    "tenno-semi-auto-rifle.png": "latron.png",
    "grineer-sawblade-weapon.png": "miter.png",
    
    # Secondary
    "akstiletto.png": "tenno-uzi.png",
    "bronco.png": "hand-cannon.png",
    "bronco-prime.png": "prime-bronco.png",
    "corpus-minigun.png": "cestra.png",
    "grineer-heavy-pistol.png": "kraken.png",
    "grn-heat-gun.png": "atomos.png",
    "grn-wind-up-pistol.png": "kohmak.png",
    "kunai-throwing-knives.png": "kunai.png",
    "lato.png": "pistol.png",
    "lex-prime.png": "prime-lex.png",
    "magnus.png": "tenno-magnum.png",
    "tenno-akimbo-pistols.png": "akbolto.png",
    "throwing-star.png": "hikou.png",
    "vasto.png": "tenno-hand-cannon.png",
    "viper.png": "grineer-light-pistol.png",
    
    # Melee
    "bo-staff.png": "bo.png",
    "dual-axe.png": "dual-zoren.png",
    "grineer-dual-cleaver.png": "dual-cleavers.png",
    "grn-egypt-sword.png": "krohkur.png",
    "heavy-staff.png": "amphis.png",
    "kronen.png": "tenno-tonfa.png",
    "melee-gauntlets.png": "furax.png",
    "nikana.png": "katana.png",
    "nunchaku.png": "ninkondi.png",
    "paladin-mace.png": "magistar.png",
    "stalker-two-sword.png": "broken-war.png",
    "tenno-gauntlet.png": "ankyros.png",
    "tenno-great-sword.png": "galatine.png",
    "tipedo.png": "tno-monk-staff.png",
    
    # Misc
    "bird-of-prey-part-item.png": "condroc-wing.png",
    "ducat.png": "orokin-ducats.png",
    "eidolonium.png": "breath-of-the-eidolon.png",
    "mandochord.png": "mandachord.png",
    
    # Resources
    "circuits.png": "component-circuits.png",
    "cryotic.png": "component-cryotic.png",
    "detonite-injector.png": "grineer-component.png",
    "ferrite.png": "component-ferrite.png",
    "fieldron.png": "corpus-component.png",
    "forma.png": "generic-component.png",
    "gallium.png": "component-gallium.png",
    "morphics.png": "component-morphic.png",
    "mutagen-mass.png": "infested-component.png",
    "nano-spores.png": "component-nanospores.png",
    "neural-sensors.png": "neural-sensor.png",
    "neurodes.png": "component-neurode.png",
    "nitain-extract.png": "alertium.png",
    "orokin-cell.png": "component-cell.png",
    "oxium.png": "component-oxium.png",
    "plastids.png": "component-plastids.png",
    "rubedo.png": "component-rubedo.png",
    "salvage.png": "component-salvage.png",

    # Plains of Eidolon
    "common-gem-bcut-a.png": "esher-devar.png",
    "coprite-alloy.png": "common-ore-balloy-a.png",
    "fersteel-alloy.png": "uncommon-ore-aalloy-a.png",
    "fish-resource-sac.png": "cuthol-tendrils.png",
    "fish-scales.png": "fish-resource-scales.png",
    "intact-sentient-core.png": "quills-common-pickup.png",
    "pyrotic-alloy.png": "common-ore-aalloy-a.png",
    "maprico.png": "eidolon-fruit.png",
    "marquise-veridos.png": "uncommon-gem-acut-a.png",
    "murkray-liver.png": "fish-resource-organ.png",
    "nistlepod.png": "nistlebrush-item.png",
    "norg-brain.png": "fish-resource-brain.png",
    "rare-ore-aalloy-a.png": "auroxium-alloy.png",
    "sentient-shard-common.png": "eidolon-shard.png",
    "star-crimzian.png": "rare-gem-acut-a.png",
    "tear-azurite.png": "common-gem-acut-a.png",

    # Orb Vallis
    "corpus-widget-b.png": "atmo-systems.png",
    "crp-arachnoid-power-core-camper.png": "calda-toroid.png",
    "fish-part-ecosynth-analyzer.png": "ecosynth-analyzer.png",
    "goblite-tears.png": "solaris-uncommon-gem-acut.png",
    "gorgaricus-spore.png": "venus-coconut.png",
    "gyromag-systems.png": "corpus-widget-a.png",
    "hespazym-alloy.png": "solaris-rare-ore-aalloy.png",
    #"lathe-coagulant.png": "microid.png",
    "mytocardia-spore.png": "fungus-heart.png",
    "radiant-zodian.png": "solaris-eidolon-gem-acut.png",
    "repeller-systems.png": "corpus-widget-c.png",
    "rotoblade.png": "fish-part-rotor-blade.png",
    "sagan-module.png": "fish-part-sagan-module.png",
    "scrap.png": "fish-part-scrap.png",
    "seram-beetle-shell.png": "fish-resource-shell.png",
    "smooth-phasmin.png": "solaris-common-gem-acut.png",
    "star-amarast.png": "solaris-rare-gem-acut.png",
    "tepa-nodule.png": "venus-tree.png",
    "thermal-laser.png": "fish-part-thermal-laser.png",
    "thermal-sludge.png": "coolant.png",
    "travocyte-alloy.png": "solaris-common-ore-aalloy.png",
    "vega-toroid.png": "crp-arachnoid-power-core-microid.png",
    "venerdo-alloy.png": "solaris-uncommon-ore-aalloy.png",
}

sigh = {
    ".png": ".png",
    ".png": ".png",

    # Orb Vallis
}

# -----------------------------------------------------------------------------

class Command(BaseCommand):
    def handle(self, *args, **options):
        self.build_js()
        self.build_images()

    def build_js(self):
        self.images = set()

        item_components = {}
        item_drops = {}
        item_ids = set()
        lines = []

        for component in ItemComponent.objects.prefetch_related():
            if component.item_id not in item_components:
                item_components[component.item_id] = []
            item_components[component.item_id].append(component)
            item_ids.add(component.component_id)

        for drop in ItemDrop.objects.exclude(rotation='Annihilation'):
            if drop.item_id not in item_drops:
                item_drops[drop.item_id] = []
            item_drops[drop.item_id].append(drop)

        lines.append('var FramethingData = {')

        categories = {}
        sub_categories = {}
        lines.append('items: {')
        for item in Item.objects.order_by('category', 'sub_category', 'name'):
            if item.category == 0 and item.id not in item_ids:
                continue

            image = IMAGE_OVERRIDES.get(item.image_name, item.image_name)
            self.images.add(image)

            item_data = dict(
                name=item.name,
                image_name=image,
                category=item.category,
                sub_category=item.sub_category,
            )

            if item.id in item_components:
                components = item_components[item.id]
                temp = []
                temp2 = []
                for component in components:
                    if not component.is_resource:
                        temp.append(['Blueprint' not in component.component.name, component.component.name, component.count, component.component_id])
                    else:
                        temp2.append([-component.count, component.component.name, component.count, component.component_id])

                temp.sort()
                temp2.sort()

                item_data['components'] = []
                #  !blueprint, name, count, component_id
                for _, _, component_count, component_id in temp:
                    for i in range(component_count):
                        item_data['components'].append([component_id, 1, False])
                # -count, name, 
                for _, _, component_count, component_id in temp2:
                    item_data['components'].append([component_id, component_count, True])

            if item.id in item_drops:
                item_data['drops'] = []

                i = 0
                while i < len(item_drops[item.id]):
                    drop = item_drops[item.id][i]
                    if drop.type == 'Relics':
                        chance = '%s-%s' % (drop.chance, item_drops[item.id][i+3].chance);
                        item_data['drops'].append([drop.location.rsplit(None, 1)[0], drop.type, drop.rotation, drop.rarity, chance])
                        i += 4
                    else:
                        item_data['drops'].append([drop.location, drop.type, drop.rotation, drop.rarity, str(drop.chance)])
                        i += 1

            lines.append("%d: %s," % (item.id, json.dumps(item_data)))
            if item.category > 0:
                categories.setdefault(item.category, []).append(item.id)
        lines.append('},')

        lines.append('categories: {')
        for cat_id, items in sorted(categories.items()):
            lines.append("%d: %r," % (cat_id, items))
        lines.append('},')

        lines.append('sub_categories: {')
        for sub_id, sub_name in Item.SUB_CATEGORY_CHOICES:
            lines.append("%d: %r," % (sub_id, sub_name))
        lines.append('},')

        lines.append('};')
        lines.append('')

        with open(OUT_PATH, 'w') as out:
            out.write('\n'.join(lines))

    def build_images(self):
        if os.environ['DJANGO_SETTINGS_MODULE'] != 'settings.local':
            return
        
        from PIL import Image

        for filename in self.images:
            src_path = os.path.join(IMAGE_SRC_PATH, filename)
            dst_path = os.path.join(IMAGE_DST_PATH, filename.replace('.jpg', '.png').replace('.jpeg', '.png'))

            if filename == 'blueprint.png':
                src_path = 'static/img/blueprint2.png'

            if os.path.exists(src_path) and not os.path.exists(dst_path):
                print("Resizing %s => %r" % (src_path, dst_path))
                im = Image.open(src_path)

                blah = im.convert(mode="RGBA")
                #blah = im.resize((80, 80), Image.BICUBIC)
                #print(blah.format, blah.mode)
                blah.thumbnail((80, 80))
                blah.save(dst_path, quality=100)
                #im.thumbnail((80, 80))
                #im.save(dst_path, quality=100)

# -----------------------------------------------------------------------------
