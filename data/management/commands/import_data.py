import json
import os
import re

from django.core.management.base import BaseCommand, CommandError

from data.manual import manual_drops
from data.models import *
from data.utils import *

# -----------------------------------------------------------------------------

JSON_PATH = 'warframe-items/data/json'

RESOURCE_PARTS = [
    '/FishParts/',
    '/Gems/',
    '/MiscItems/',
    '/Research/',
    '/Resources/',
]

GARBAGE_RE = re.compile(r' \d+\.\d+$')

# -----------------------------------------------------------------------------

class Command(BaseCommand):
    def handle(self, *args, **options):
        if not os.path.exists(JSON_PATH):
            raise CommandError('Path "%s does not exist!' % (JSON_PATH))

        self.all_data = json.load(open(os.path.join(JSON_PATH, 'All.json'), encoding="utf-8"))
        self.category_map = {cat_name: cat_id for cat_id, cat_name in Item.CATEGORY_CHOICES}
        self.sub_category_map = {sub_name: sub_id for sub_id, sub_name in Item.SUB_CATEGORY_CHOICES}
        self.drops = {}

        self.do_items()
        self.do_item_components()
        self.do_item_drops()

    # -------------------------------------------------------------------------

    def do_items(self):
        # Part 1: items
        item_map = self.get_item_map()
        new = []

        for item_data in self.all_data:
            category, sub_category = self.categorize(item_data)

            item = item_map.get(item_data['uniqueName'])
            if item is None:
                new.append(Item(
                    name=item_data['name'],
                    unique_name=item_data['uniqueName'],
                    image_name=item_data.get('imageName', 'adelfos-selene-glyph'),
                    category=category,
                    sub_category=sub_category,
                    build_price=item_data.get('buildPrice', 0),
                    build_time=item_data.get('buildTime', 0),
                    skip_price=item_data.get('skipBuildTimePrice', 0),
                ))

            else:
                updated_fields = update_fields(item, dict(
                    name=item_data['name'],
                    unique_name=item_data['uniqueName'],
                    image_name=item_data.get('imageName', 'adelfos-selene-glyph'),
                    category=category,
                    sub_category=sub_category,
                    build_price=item_data.get('buildPrice', 0),
                    build_time=item_data.get('buildTime', 0),
                    skip_price=item_data.get('skipBuildTimePrice', 0),
                ))
                if updated_fields:
                    item.save(update_fields=updated_fields)

            self.gather_drops(item_data)

        Item.objects.bulk_create(new)

        # Part 2: item components, many of which don't exist in the base data
        item_map = self.get_item_map()
        new = []

        for item_data in self.all_data:
            for component in item_data.get('components', []):
                # Chroma is weird.
                #if item_data['uniqueName'] == "/Lotus/Powersuits/Dragon/Dragon" and component['uniqueName'] == "/Lotus/Types/Recipes/WarframeRecipes/VOLTHelmetComponent":
                #    continue

                # Recipe component names need to be updated from "Blueprint" to "Item Blueprint"
                name = component['name']
                if self.is_recipe(component['uniqueName']):
                    name = "%s %s" % (item_data['name'], component['name'])

                item = item_map.get(component['uniqueName'])
                if item is None:
                    new.append(Item(
                        name=name,
                        unique_name=component['uniqueName'],
                        image_name=component.get('imageName', 'adelfos-selene-glyph'),
                        tradable=component.get('tradable', False),
                        ducats=component.get('ducats', 0),
                    ))
                    item_map[component['uniqueName']] = True

                elif item is not True:
                    updated_fields = update_fields(item, dict(
                        name=name,
                        unique_name=component['uniqueName'],
                        image_name=component.get('imageName', 'adelfos-selene-glyph'),
                        tradable=component.get('tradable', False),
                        ducats=component.get('ducats', 0),
                    ))
                    if updated_fields:
                        item.save(update_fields=updated_fields)

                self.gather_drops(component)

        Item.objects.bulk_create(new)

    def is_recipe(self, name):
        if "/WeaponParts/ArchHeavyPistols" in name:
            return False

        if "/Recipes/" in name:
            return True
        
        if "/ClanTech/" in name and name.endswith('Blueprint'):
            return True

        return False

    # -------------------------------------------------------------------------

    def do_item_components(self):
        # Updating component state is a giant pain, let's just not bother
        self.truncate_table(ItemComponent._meta.db_table)

        item_map = self.get_item_map()
        new = []

        for item_data in self.all_data:
            item = item_map.get(item_data['uniqueName'])
            if item is None or item.category == 0:
                continue

            seen = {}
            for component_data in item_data.get('components', []):
                component_item = item_map.get(component_data['uniqueName'])
                if component_item is None:
                    print("Missing component %s on item %s", (component_data['uniqueName'], item.unique_name))
                    continue

                is_resource = False
                for part in RESOURCE_PARTS:
                    if part in component_data['uniqueName']:
                        is_resource = True
                        break

                new.append(ItemComponent(
                    item=item,
                    component=component_item,
                    count=component_data['itemCount'],
                    is_resource=is_resource,
                ))

        ItemComponent.objects.bulk_create(new)

    # -------------------------------------------------------------------------

    def do_item_drops(self):
        # Updating drop state is a giant pain, let's just not bother
        self.truncate_table(ItemDrop._meta.db_table)

        item_map = self.get_item_map()
        new = []

        for unique_name, drop_data in self.drops.items():
            item = item_map.get(unique_name)
            if item is None:
                continue

            if item.name in manual_drops:
                continue

            for drop in drop_data:
                chance = drop['chance']
                if chance is None:
                    continue

                location = drop['location']
                while True:
                    replaced = GARBAGE_RE.sub('', location)
                    if replaced == location:
                        break
                    location = replaced

                # if len(drop['location']) > 32:
                #     print('location', len(drop['location']), drop['location'])
                # if len(drop['type']) > 32:
                #     print('type', len(drop['type']), drop['type'])

                new.append(ItemDrop(
                    item=item,
                    location=location,
                    type=drop['type'],
                    rotation=drop.get('rotation'),
                    rarity=drop.get('rarity'),
                    chance=drop['chance'] * 100,
                ))

        # Manual entries
        name_map = {}
        for item in Item.objects.filter(name__in=manual_drops.keys()):
            name_map[item.name] = item

        for item_name, drop_data in manual_drops.items():
            item = name_map.get(item_name)
            if item is None:
                continue

            for location, drop_type, rarity in drop_data:
                new.append(ItemDrop(
                    item=item,
                    location=location,
                    type=drop_type,
                    rotation=None,
                    rarity=rarity,
                    chance=100,
                ))

        ItemDrop.objects.bulk_create(new)

    # -------------------------------------------------------------------------

    def truncate_table(self, table):
        from django.db import connection
        cursor = connection.cursor()
        cursor.execute("TRUNCATE TABLE %s RESTART IDENTITY" % (table))

    # -------------------------------------------------------------------------

    def gather_drops(self, item_data):
        if 'drops' not in item_data or item_data['uniqueName'] in self.drops:
            return

        self.drops[item_data['uniqueName']] = item_data.get('drops', [])

    # -------------------------------------------------------------------------

    def get_item_map(self):
        item_map = {}
        for item in Item.objects.all():
            item_map[item.unique_name] = item
        return item_map

    # -------------------------------------------------------------------------

    def categorize(self, item_data):
        category = 0
        sub_category = 0
        slot = item_data.get('slot', -1)
        unique_name = item_data['uniqueName']

        if slot >= 0:
            if '/Archwing/Melee/' in unique_name:
                category = self.category_map['Archwing Melee Weapon']
            
            elif '/Archwing/Primary/' in unique_name:
                category = self.category_map['Archwing Primary Weapon']
            
            elif '/Sentinels/SentinelWeapons/' in unique_name:
                category = self.category_map['Sentinel Weapon']

            elif '/MoaPetComponents/' in unique_name and unique_name.endswith('Weapon'):
                category = self.category_map['Sentinel Weapon']

            elif slot == 0:
                category = self.category_map['Secondary Weapon']
            
            elif slot == 1:
                category = self.category_map['Primary Weapon']
            
            elif slot == 5 and item_data['name'] != 'Zaw':
                category = self.category_map['Melee Weapon']
            
            else:
                print(slot, unique_name, item_data['name'])
        
        elif '/Powersuits/Archwing/' in unique_name and not unique_name.endswith('AugmentCard'):
            category = self.category_map['Archwing']

        elif '/Powersuits/' in unique_name and not unique_name.endswith('AugmentCard'):
            category = self.category_map['Warframe']
        
        # Companions
        elif unique_name.endswith('CatbrowPetPowerSuit'):
            category = self.category_map['Companion']
            sub_category = self.sub_category_map['Kavat']

        elif unique_name.endswith('KubrowPetPowerSuit'):
            category = self.category_map['Companion']
            sub_category = self.sub_category_map['Kubrow']
        
        elif '/SentinelPowersuits/' in unique_name:
            category = self.category_map['Companion']
            sub_category = self.sub_category_map['Sentinel']

        # Custom Items
        elif '/HoverboardParts/' in unique_name and unique_name.endswith('Deck'):
            category = self.category_map['Custom Item']
            sub_category = self.sub_category_map['K-Drive Board']

        elif '/SUModularSecondarySet1/Barrel/' in unique_name and not unique_name.endswith('Blueprint'):
            category = self.category_map['Custom Item']
            sub_category = self.sub_category_map['Kitgun Chamber']

        elif '/MoaPetHead' in unique_name and not unique_name.endswith('Blueprint'):
            category = self.category_map['Custom Item']
            sub_category = self.sub_category_map['Moa Head']

        elif '/OperatorAmplifiers/' in unique_name and 'Barrel' in unique_name and not unique_name.endswith('Blueprint'):
            category = self.category_map['Custom Item']
            sub_category = self.sub_category_map['Operator Amp']

        elif '/Ostron/Melee/ModularMelee' in unique_name and '/Tip' in unique_name:
            category = self.category_map['Custom Item']
            sub_category = self.sub_category_map['Zaw Tip']

        return [category, sub_category]

# -----------------------------------------------------------------------------
