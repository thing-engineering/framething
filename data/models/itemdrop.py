from django.db import models

from .item import Item

class ItemDrop(models.Model):
    item = models.ForeignKey(Item, related_name='drops', on_delete=models.CASCADE)
    location = models.CharField(max_length=64)
    type = models.CharField(max_length=64)
    rotation = models.CharField(max_length=64, blank=True, null=True)
    rarity = models.CharField(max_length=10, blank=True, null=True)
    chance = models.DecimalField(max_digits=4, decimal_places=1)
