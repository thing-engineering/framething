from django.db import models

from .item import Item

class ItemComponent(models.Model):
    item = models.ForeignKey(Item, related_name='+', on_delete=models.CASCADE)
    component = models.ForeignKey(Item, related_name='+', on_delete=models.CASCADE)
    count = models.IntegerField()
    is_resource = models.BooleanField()

    def __str__(self):
        return "%s x%d" % (self.component.name, self.count)
