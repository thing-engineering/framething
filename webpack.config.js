var path = require('path');
var webpack = require('webpack');

module.exports = {
  context: __dirname,
  entry: './static/js/framething.js',
  output: {
      path: path.resolve('./static/js/webpack/'),
      filename: "framething.js"
  },
}
