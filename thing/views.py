import json
import re

from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.http import HttpResponse
from django.shortcuts import *
from django.template import RequestContext
from django.views.decorators.http import *

from data.models import Item
from data.utils import *
from thing.models import UserProgress

# -----------------------------------------------------------------------------

PARAM_RE = re.compile(r'^(\d+)\[([fmo])\]$')
PARAM_ARRAY_RE = re.compile(r'^(\d+)\[([c])\]\[\]$')

# -----------------------------------------------------------------------------

def index(request):
	return render(request, 'thing/index.html')

# -----------------------------------------------------------------------------

def user_code(request, code):
    user = get_object_or_404(User, userprofile__code=code)

    progress = {}
    for up in UserProgress.objects.filter(user=user):
        progress[up.item_id] = dict(o=up.owned, m=up.mastered, f=up.forma_count, c=up.components)

    return render(request, 'thing/user.html', dict(
        progress=json.dumps(progress, separators=(', ', ':')),
        public=user != request.user,
    ))

# -----------------------------------------------------------------------------

@login_required
def home_redirect(request):
    return redirect('user_code', code=request.user.userprofile.code)

# -----------------------------------------------------------------------------

@login_required
@require_POST
def update_progress(request):
    data = {}
    component_ids = set()
    for k, v in request.POST.items():
        m = PARAM_RE.match(k)
        if m:
            data.setdefault(int(m.group(1)), {})[m.group(2)] = v
        else:
            m = PARAM_ARRAY_RE.match(k)
            if m:
                sigh = [maybe_int(n) for n in request.POST.getlist(k)]
                component_ids = component_ids.union(sigh)
                data.setdefault(int(m.group(1)), {})[m.group(2)] = sigh
            else:
                print('POST:', k, '=>', v)

    valid_ids = set(Item.objects.filter(id__in=component_ids.union(data.keys())).values_list('id', flat=True))

    up_map = {}
    for up in UserProgress.objects.filter(user=request.user, item__in=valid_ids):
        up_map[up.item_id] = up

    new = []
    for item_id, item_data in data.items():
        if item_id not in valid_ids:
            continue

        #print(item_data.get('c', []))
        
        mastered = item_data.get('m', 'false') == 'true'
        owned = item_data.get('o', 'false') == 'true'
        forma_count = maybe_int(item_data.get('f', '0'))
        components = list(sorted([c for c in item_data.get('c', []) if c in valid_ids]))

        up = up_map.get(item_id, None)
        if up is None:
            new.append(UserProgress(
                user=request.user,
                item_id=item_id,
                mastered=mastered,
                owned=owned,
                forma_count=forma_count,
                components=components,
            ))
        
        else:
            updated_fields = update_fields(up, dict(
                mastered=mastered,
                owned=owned,
                forma_count=forma_count,
                components=components,
            ))
            if updated_fields:
                up.save(update_fields=updated_fields)

    UserProgress.objects.bulk_create(new)

    return HttpResponse("success")

# -----------------------------------------------------------------------------
