from django.contrib.auth.models import User
from django.contrib.postgres.fields import ArrayField
from django.db import models

class UserProgress(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    item = models.ForeignKey('data.Item', on_delete=models.CASCADE)
    owned = models.BooleanField(default=False)
    mastered = models.BooleanField()
    forma_count = models.SmallIntegerField()
    components = ArrayField(models.IntegerField())
