#!/bin/sh

python manage.py migrate || exit 1
python manage.py import_data || exit 1
python manage.py build_js || exit 1
python manage.py collectstatic --noinput || exit 1
python manage.py compress || exit 1
python manage.py collectstatic --noinput || exit 1

exec gunicorn framething.wsgi:application \
    --name framething \
    --bind 0:8000 \
    --log-level info
