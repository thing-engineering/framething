FROM nikolaik/python-nodejs:python3.7-nodejs10

WORKDIR /app
COPY requirements/ requirements/
RUN pip install -r requirements/production.txt

COPY package*.json ./
RUN npm install

COPY . .

ENV DJANGO_SETTINGS_MODULE settings.production
ENTRYPOINT ["./run.sh"]
